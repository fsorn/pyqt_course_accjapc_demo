"""
High-level tests for the  package.

"""

import pyqt_course_code_gen


def test_version():
    assert pyqt_course_code_gen.__version__ is not None
