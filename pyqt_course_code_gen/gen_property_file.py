import os
from accjapc.codegen import codegen
from pyqt_course_code_gen import SinusPapcDevice


def write_to_file(prop_name: str, code: str):
    directory = os.path.dirname(os.path.abspath(__file__))
    file_name = f'{prop_name.capitalize()}Property.py'
    location = os.path.join(directory, file_name)
    with(open(location, 'w+')) as out_file:
        out_file.write(code)
    print(f'Device Property File generated in {file_name}')


if __name__ == '__main__':
    class_gen = next(codegen((SinusPapcDevice.device_property_data(),
                              SinusPapcDevice.property)))
    write_to_file(SinusPapcDevice.property, class_gen)
