from datetime import datetime
from typing import NamedTuple, Union, Dict
from math import pi

from papc.device import Device
from papc.deviceproperty import Setting
from papc.fieldtype import FieldType
from papc.simulator.trig import RepeatedTimer
from accjapc import DevicePropertyData
import numpy as np


class FieldTuple(NamedTuple):
    """
    Named Tuple which allows a bit more convenient access to a fields
    definition. A field is defined by its name and a initial value, from
    which the field data type will automatically be looked up from.
    """
    name: str
    initial_value: Union[np.ndarray, float]


def sin(amplitude: float = 1.0) -> np.ndarray:
    """
    Generate a sinus curve as a 2d numpy array using the passed amplitude.
    Both arrays will contain 200 entries. The first array will be the x
    values of the curve, while the other one will contain the corresponding
    y values.
    """
    now = datetime.now().timestamp()
    curve_x = np.linspace(0, 4 * pi, 200) + now
    curve_y = np.sin(curve_x) * amplitude
    return curve_y


class SinusPapcDevice(Device):

    def __init__(self, frequency: int = 30):
        """
        Simulated Device, which offers a single property, which describes
        a sinus curve. There are three fields, which can be interacted with.

        This device contains the following property and fields.

        Property 'sinus':
            - Field 'Amplitude':    Amplitude of the Sinus Curve
            - Field 'Curve':        The entire stored curve as x and y values
            - Field 'Point':        The most recent y value of the field curve

        Args:
            frequency: How often per seconds are updates coming?
        """
        amplitude_field = FieldType(name=SinusPapcDevice.amplitude.name,
                                    initial_value=SinusPapcDevice.amplitude.initial_value,
                                    datatype=type(SinusPapcDevice.amplitude.initial_value).__name__)
        curve_field = FieldType(name=SinusPapcDevice.curve.name,
                                initial_value=SinusPapcDevice.curve.initial_value,
                                datatype=type(SinusPapcDevice.curve.initial_value).__name__)
        point_field = FieldType(name=SinusPapcDevice.point.name,
                                initial_value=SinusPapcDevice.point.initial_value,
                                datatype=type(SinusPapcDevice.point.initial_value).__name__)
        sinus_property = Setting(name=SinusPapcDevice.property,
                                 fields=(curve_field, point_field, amplitude_field))
        super().__init__(name=SinusPapcDevice.__name__,
                         device_properties=(sinus_property,))
        self.frequency = frequency
        self._timer = RepeatedTimer(1 / self.frequency, self.update_fields)

    def update_fields(self):
        """
        Callback as soon as the timer fires. This will read the current state
        of the amplitude field and set the curve and point fields accordingly.
        """
        # get the current state of the field amplitude
        field_name = f'{SinusPapcDevice.property}#{SinusPapcDevice.amplitude.name}'
        state: Dict = self.get_state([field_name], timing_selector='')
        amplitude = state[field_name]['value']
        s = sin(amplitude)
        # Update states with the new sinus curve
        self.set_state({f'{SinusPapcDevice.property}#{SinusPapcDevice.curve.name}': s}, '')
        self.set_state({f'{SinusPapcDevice.property}#{SinusPapcDevice.point.name}': s[-1]}, '')

    amplitude = FieldTuple(name='amplitude', initial_value=5.0,)
    curve = FieldTuple(name='curve', initial_value=sin())
    point = FieldTuple(name='point', initial_value=float(sin()[-1]))

    property = 'sinus'
    fields = (amplitude, curve, point)

    @staticmethod
    def device_property_data() -> DevicePropertyData:
        """
        Generate a device property data dictionary from the devices fields.
        The resulting dictionary will look like this:

        {
            'amplitude': 5.0,

            'curve': np.array([[...], [...]]),

            'point': 0.0,
        }

        Returns:
            DevicePropertyData Dictionary containing the devices fieldnames as
        """
        data_sample = {}
        for field in SinusPapcDevice.fields:
            data_sample[field.name] = field.initial_value
        return data_sample
