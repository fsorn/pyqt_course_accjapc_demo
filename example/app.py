from datetime import datetime

# For our gui application
import numpy as np
from qtpy import QtWidgets, QtCore
import accwidgets.graph as accgraph

# For the initialization of Papc
from papc.interfaces.pyjapc import SimulatedPyJapc
from papc.system import System

# The Papc Device Definition
from pyqt_course_code_gen import SinusPapcDevice
from pyqt_course_code_gen import SinusDeviceProperty
from accjapc import ControlEndpointAddress


def make_papc() -> SimulatedPyJapc:
    """
    Create a system for papc and create a simulated pyjapc
    which our DeviceProperty can use to acquire and set data.

    Returns:
        Papc instance which is able to replace pyjapc for simulation purposes
    """
    return SimulatedPyJapc.from_simulation_factory(
        lambda: System(devices=[SinusPapcDevice()]),
        strict=False
    )()


class Win(QtWidgets.QMainWindow):

    def __init__(self, parent: QtWidgets.QWidget = None):
        """
        Example window which allows monitoring and changing settings
        of the Papc Device.

        Args:
            parent: Parent object for the window
        """
        super().__init__(parent=parent)
        endpoint = ControlEndpointAddress(device=SinusPapcDevice.__name__,
                                          prop=SinusPapcDevice.property)
        # Instantiate the generated property file with our simulated
        # japc instance and the endpoint, where we want to reach this device
        self._device_property = SinusDeviceProperty(
            japc=make_papc(),
            addr=endpoint)
        # Widgets for the window content
        self._scrolling = accgraph.ScrollingPlotWidget()
        self._static = accgraph.StaticPlotWidget()
        self._slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self._config_widgets()
        self._setup_layout()
        self._device_property.subscribe()
        self.show()

    def _config_widgets(self):
        """
        Configure the slider and both plots.
        """
        # Slider Configuration
        self._slider.setMaximum(10.0)
        self._slider.setMinimum(0.0)
        self._slider.setTickInterval(1.0)
        self._slider.setTickPosition(QtWidgets.QSlider.TicksBelow)
        self._slider.setValue(SinusPapcDevice.amplitude.initial_value)
        self._slider.setTickInterval(0.1)
        self._slider.valueChanged.connect(self._device_property.set_amplitude)
        # Plot Configuration
        point_source = accgraph.SignalBoundDataSource(
            sig=self._device_property.point_updated,
            data_type=accgraph.PointData)
        self._scrolling.addCurve(data_source=point_source)
        self._scrolling.setRange(yRange=(-10, 10))
        curve_source = accgraph.SignalBoundDataSource(
            sig=self._device_property.curve_updated,
            data_type=accgraph.CurveData)
        self._static.addCurve(data_source=curve_source)
        self._static.setRange(yRange=(-10, 10))

    def _setup_layout(self):
        """
        Put the created widget in a vertical layout and add it to the window
        """
        cw = QtWidgets.QWidget()
        cl = QtWidgets.QVBoxLayout()
        cl.addWidget(QtWidgets.QLabel(f'Append new value from '
                                      f'{SinusPapcDevice.property}#{SinusPapcDevice.point.name}'))
        cl.addWidget(self._scrolling)
        cl.addWidget(QtWidgets.QLabel(f'Replace curve with '
                                      f'{SinusPapcDevice.property}#{SinusPapcDevice.curve.name}'))
        cl.addWidget(self._static)
        cl.addWidget(QtWidgets.QLabel(f'Sets field '
                                      f'{SinusPapcDevice.property}#{SinusPapcDevice.amplitude.name} '
                                      f'(0 - 10):'))
        cl.addWidget(self._slider)
        cw.setLayout(cl)
        self.setCentralWidget(cw)


def main():
    app = QtWidgets.QApplication([])
    _ = Win()
    app.exec()


if __name__ == '__main__':
    main()
