# PyQt-Course: AccJapc Example application

This repositry contains a small example for building a gui application using
code generated with accjapc in combination with papc, a pyjapc simulation framework.

The examples are built with a simulated device that has a single property "sinus"
that holds different information about a single sinus curve. Additionally this sinus
curve can be manipulated through a field "amplitude", which allows setting the
amplitude of the generated sinus curve.

The values of the sinus curve can be acquired using two differnt fields:
- curve: Returns the entire Sinus Curve as a 2 dimensional numpy array
- point: Returns the last y value of the curve as a single float

The provided example application allows monitoring these two fields in a plot.
The first one appends the most recent value of 'point', while keeping the old
ones it received before without any changes. The second displays the current
value of the field 'curve' and does not keep any old values.

At the bottom there is a slider which allows manipulation of the field 'amplitude'.
The effects are then visible in the two plots.

## Installation and running examples

Dependencies are listed in [setup.py](./setup.py), so they can simply be installed using
```
git checkout https://gitlab.cern.ch/fsorn/pyqt_course_accjapc_demo.git
pip install -e .
```

Example can be run by executing:
```
python examples/app.py
```

